FROM ubuntu:latest

RUN ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
        git latexdiff \
        texlive texlive-science texlive-extra-utils texlive-lang-german latexmk \
        texlive-bibtex-extra biber && \
    dpkg-reconfigure --frontend noninteractive tzdata && \
    apt-get clean
COPY ./git-latexdiff /

WORKDIR /development
ENTRYPOINT [ "/git-latexdiff" ]